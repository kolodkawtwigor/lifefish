package{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;

	public class FishModel extends EventDispatcher implements IFishModel
	{	
		private var __foodTimer:Timer;
		protected var __kall:int;
		protected var __moveXY:Point;
		private var __moveTimer:Timer;
		protected var __totalKall:int;
		private var __timerGold:Timer;
		
		public function FishModel()
		{
		}
		
		public function get totalKall():int
		{
			return __totalKall;
		}
		
		public function get moveXY():Point
		{
			return __moveXY;
		}

		public function get kall():int
		{
			return __kall;
		}

		public function set kall(value:int):void
		{
			if(value  >= __totalKall) value = __totalKall 
			else if ( value  <= 0) value = 0;

			__kall = value;
			this.dispatchEvent( new Event(MyEvents.EAT_FISH));
			if(value == 0) dispatchEvent(new Event(MyEvents.GAME_OVER)) ;
			
			
		}

		public function moveTo():void
		{
			__moveTimer = new Timer(6000);
			__moveTimer.addEventListener(TimerEvent.TIMER, randomXY);
			__moveTimer.start();
		}
		
		protected function randomXY(event:TimerEvent):void
		{
			__moveXY = new Point();
			__moveXY.x = 500*Math.random();
			__moveXY.y = 100+500*Math.random();
			this.dispatchEvent( new Event(MyEvents.GO_TO_POINT));
		}
		
		public function startLife():void
		{
			__foodTimer = new Timer(1000);
			__foodTimer.addEventListener(TimerEvent.TIMER, minusKall);
			__foodTimer.start();
			moveTo();
			timerGold();
		}
		
		protected function minusKall(e:TimerEvent):void
		{
			kall = __kall - 1;	
		}
		
		public function eat(countKall:int):void
		{
		}
		
		public function timerGold():void
		{
			__timerGold = new Timer(6000);
			__timerGold.addEventListener(TimerEvent.TIMER, creatGold);
			__timerGold.start();
		}
		
		protected function creatGold(event:TimerEvent):void
		{
			dispatchEvent( new Event( MyEvents.CREATE_GOLD));
		}
		
		public function destroy():void
		{
			__timerGold.removeEventListener(TimerEvent.TIMER, creatGold);
			__foodTimer.removeEventListener(TimerEvent.TIMER, minusKall);
			__moveTimer.removeEventListener(TimerEvent.TIMER, randomXY);
		}
	}
}