package{
	import Screen.MapScreen;
	
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;

	public class FishView extends Sprite
	{
		private var __fishModel:IFishModel;
		public var fish:ClowFish_GUI;
		private var __gold:Sprite;
		
		public function FishView(fishModel:IFishModel)
		{
			__fishModel = fishModel;
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);		
		}
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			createFish();
			__fishModel.addEventListener(MyEvents.GO_TO_POINT, goToPoint);
			__fishModel.addEventListener(MyEvents.GAME_OVER, killFish);
		}
		
		private function killFish(e:Event):void
		{
			TweenMax.to(this, 0.5, {alpha: 0, onComplete: completeTween });
		}
		
		private function completeTween():void
		{
			ScreenManager.instance.show(MapScreen);
		}
		
		private function goToPoint(e:Event):void
		{
			if(x >= __fishModel.moveXY.x) scaleX = -1;
			else scaleX = 1;
			TweenLite.to(this, 6, { x: __fishModel.moveXY.x, y:__fishModel.moveXY.y});
		}
		
		public function createFish():void
		{
			fish = new ClowFish_GUI;
			this.addChild(fish);
		}
		
		private function destroy(event:Event):void
		{
			__fishModel.removeEventListener(MyEvents.GO_TO_POINT, goToPoint);
			__fishModel.removeEventListener(MyEvents.GAME_OVER, killFish);
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}