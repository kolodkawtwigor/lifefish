package{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Sine;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	public class Gold extends Gold_GUI
	{
		private var __panelModel:PanelModel;
		private var __timerDeleteGold:Timer;
		
		public function Gold(panelModel:PanelModel)
		{
			createtimer();
			__panelModel = panelModel;
			deleteGold();
			this.buttonMode = true;
		}
		
		private function createtimer():void
		{
			__timerDeleteGold = new Timer(1000, 4);
			__timerDeleteGold.start();
			__timerDeleteGold.addEventListener(TimerEvent.TIMER_COMPLETE, removeTimerGold);
		}
		
		protected function removeTimerGold(event:TimerEvent):void
		{
			__timerDeleteGold.removeEventListener(TimerEvent.TIMER_COMPLETE, removeTimerGold);
			TweenMax.to(this, 0.5, {alpha: 0, onComplete: completeTween });		
		}
		
		private function completeTween():void
		{
			parent.removeChild(this);
		}
		
		private function deleteGold():void
		{
			this.addEventListener(MouseEvent.CLICK, removeGold);
		}
		
		protected function removeGold(event:MouseEvent):void
		{
			removeTimerGold(null);
			__panelModel.getGold();
		}
	}
}