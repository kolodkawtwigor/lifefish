package
{
	import flash.events.Event;

	public class GoldFishModel extends FishModel implements IFishModel
	{
		private var __panelModel:PanelModel;
		
		public function GoldFishModel(panelModel:PanelModel)
		{
			__panelModel = panelModel;
			__panelModel.addEventListener(MyEvents.FOOD, onFood);
			__panelModel.addEventListener(MyEvents.FOOD1, onFood1);
			__panelModel.addEventListener(MyEvents.FOOD2, onFood2);
			__kall = 50;
			__totalKall = __kall;
		}
		
		protected function onFood(event:Event):void
		{
			if(__panelModel.points >= 90) eat(18);
		}
		
		protected function onFood1(event:Event):void
		{
			if(__panelModel.points >= 60) eat(12);
		}
		protected function onFood2(event:Event):void
		{
			if(__panelModel.points >= 30) eat(5);
		}
		
		override public function eat(countKall:int):void
		{
			kall = __kall + countKall*1.1;
		}
		
		override public function destroy():void
		{
			super.destroy();
			__panelModel.removeEventListener(MyEvents.FOOD, onFood);
			__panelModel.removeEventListener(MyEvents.FOOD1, onFood1);
			__panelModel.removeEventListener(MyEvents.FOOD2, onFood2);
		}
	}
}