package{
	import flash.events.IEventDispatcher;
	import flash.geom.Point;

	public interface IFishModel extends IEventDispatcher
	{
		function eat(countKall:int):void;
		function startLife():void;
		function timerGold():void;
		
		function get kall():int;
		function set kall(value:int):void;
		
		function get moveXY():Point;
		
		function get totalKall():int;
		
		function destroy():void;
		
	}
}