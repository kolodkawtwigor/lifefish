package
{
	import Screen.GameScreen;
	import Screen.MapScreen;
	
	import com.greensock.TweenLite;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	[SWF(width="760", height="700", frameRate="60")]
	public class LifeFish extends Sprite
	{
		static private var __init:LifeFish;
		static public function get instance():LifeFish{ return __init; }
		
		public function LifeFish()
		{
			__init = this;
			ScreenManager.instance.show(MapScreen);
		}	
	}
}