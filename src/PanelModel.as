package{
	import flash.events.Event;
	import flash.events.EventDispatcher;

	public class PanelModel extends EventDispatcher
	{
		private var __points:uint;
		
		public function PanelModel()
		{
		}
		
		public function get points():uint
		{
			return __points;
		}

		public function set points(value:uint):void
		{
			__points = value;
			dispatchEvent(new Event(MyEvents.POINTS));
		}

		public function pressFood():void
		{
			if(points >= 90)
			{
			dispatchEvent( new Event(MyEvents.FOOD));
			points = __points - 90;
			}
		}
		public function pressFood1():void
		{
			if(points >= 60)
			{
			dispatchEvent( new Event(MyEvents.FOOD1));
			points = __points - 60;
			}
		}
		public function pressFood2():void
		{
			if(points >= 30)
			{
				dispatchEvent( new Event(MyEvents.FOOD2));
				points = __points - 30;
			}
		}
		
		public function getGold():void
		{
			points = __points + 30;
		}
	}
}