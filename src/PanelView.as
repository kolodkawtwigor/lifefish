package{
	import com.greensock.TweenLite;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;

	public class PanelView extends Panel_GUI
	{
		private var __panelModel:PanelModel;
		private var __button: Sprite;
		private var __button1:Sprite;
		private var __button2:Sprite;
		private var __hP:     Sprite;
		private var __fishModel:IFishModel;
		private var __pointText:TextField;
		private var __textButton:TextField;
		private var __textButton1:TextField;
		private var __textButton2:TextField;
		
		public function PanelView(panelModel:PanelModel, fishModel:IFishModel)
		{
			__fishModel = fishModel;
			__panelModel = panelModel;
			createPanel();
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function createPanel():void
		{
			createButtons();
		}
		
		private function createButtons():void
		{
			this.btn_food_90.label = "Еда за 90 очков";
			this.btn_food_60.label = "Еда за 60 очков";
			this.btn_food_30.label = "Еда за 30 очков";
			
			btn_food_90.addEventListener(MouseEvent.CLICK, onClick);
			btn_food_60.addEventListener(MouseEvent.CLICK, onClick1);
			btn_food_30.addEventListener(MouseEvent.CLICK, onClick2);
			
			this.btn_food_90.buttonMode = true;
			this.btn_food_60.buttonMode = true;
			this.btn_food_30.buttonMode = true;

			__hP = new Sprite;
			__hP.graphics.beginFill(0x009900);
			__hP.graphics.drawRect(0, 0, 194, 20);
			__hP.graphics.endFill();
			addChild(__hP);
			__fishModel.addEventListener(MyEvents.EAT_FISH, eatFish);

			__pointText = new TextField;
			__pointText.text ="Очки :"+ __panelModel.points.toString();
			__pointText.background = true;
			__pointText.backgroundColor = 0xCCCCCC;
			__pointText.border = true;
			__pointText.borderColor = 0x333333
			__pointText.autoSize = TextFieldAutoSize.LEFT;
			__pointText.y = 50;
			__pointText.selectable = false;
			__pointText.mouseEnabled= false;
			__panelModel.addEventListener(MyEvents.POINTS, pointChang);
			this.addChild(__pointText);
		}
			
		protected function pointChang(event:Event):void
		{		
			__pointText.text ="Очки :"+__panelModel.points.toString()
		}
		
		protected function eatFish(event:Event):void
		{
			__hP.scaleX = __fishModel.kall/__fishModel.totalKall
		}
		
		protected function onClick(event:MouseEvent):void
		{
			__panelModel.pressFood();	
		}
		protected function onClick1(event:MouseEvent):void
		{
			__panelModel.pressFood1();	
		}
		protected function onClick2(event:MouseEvent):void
		{
			__panelModel.pressFood2();	
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function destroy(event:Event):void
		{
			__panelModel.removeEventListener(MyEvents.POINTS, pointChang);
			__fishModel.removeEventListener(MyEvents.EAT_FISH, eatFish);
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}