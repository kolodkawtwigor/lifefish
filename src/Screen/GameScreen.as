package Screen
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;

	public class GameScreen extends GameScreen_GUI
	{
		private var __fishModel:IFishModel;
		private var __fishView:FishView;
		private var __panelView:PanelView;
		private var __panelModel:PanelModel;
		
		private var points:uint;
		
		public function GameScreen()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			

			__panelModel= new PanelModel();
			__fishModel = new GoldFishModel(__panelModel);
			
			__panelView= new PanelView(__panelModel, __fishModel);
			__panelView.x = 566;
			__panelView.y = 0;
			__fishView = new FishView(__fishModel);
			__fishView.x = (stage.stageWidth - __fishView.width) >> 1;
			__fishView.y = (stage.stageHeight - __fishView.height) >> 1;
			
			addChild(__fishView);
			addChild(__panelView); 

			__fishModel.startLife();
			__fishModel.addEventListener(MyEvents.CREATE_GOLD, creteGold);
			__fishModel.addEventListener(MyEvents.GAME_OVER, gameOver);
		}
		
		private function gameOver(e:Event):void
		{
			__fishModel.destroy();
		}
		
		public function creteGold(event:Event):void
		{
			var __gold:Gold = new Gold(__panelModel);
			addChild(__gold); 
			__gold.x = __fishView.x;		
			__gold.y = __fishView.y;
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			__fishModel.removeEventListener(MyEvents.CREATE_GOLD, creteGold);
			__fishModel.removeEventListener(MyEvents.GAME_OVER, gameOver);
		}
	}
}