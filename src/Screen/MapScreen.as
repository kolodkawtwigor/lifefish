package Screen
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class MapScreen extends MapScreen_GUI
	{
		private var __startGame:StartGame_GUI;
		public function MapScreen()
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			createButton();
			
		}
		
		private function createButton():void
		{			
			__startGame = new StartGame_GUI;
			__startGame.buttonMode = true;
			__startGame.x = 380;
			__startGame.y= 600;
			__startGame.b
			addChild(__startGame);
			__startGame.addEventListener(MouseEvent.CLICK, startGame);
		}
		
		protected function startGame(event:MouseEvent):void
		{
			ScreenManager.instance.show(GameScreen);
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}