package 
{
	import flash.display.Sprite;
	
	public class ScreenManager extends Sprite
	{
		private var _currentScreen:Sprite;
		
		static private var __init:ScreenManager;
		private var __screenClass:Class;
		
		static public function get instance():ScreenManager
		{
			if(!__init) __init = new ScreenManager();
			return __init;
		}
		
		public function ScreenManager()
		{
			super();
		}
		
		public function show(screenClass:Class):void
		{	
			if(_currentScreen) LifeFish.instance.removeChild(_currentScreen); 
			_currentScreen = new screenClass();
			__screenClass = screenClass;
			LifeFish.instance.addChild(_currentScreen);
		}
		
		public function get screenName():Class
		{
			return __screenClass;
		}
		
		public function destroy():void
		{
			if(_currentScreen) LifeFish.instance.removeChild(_currentScreen);
			_currentScreen = null;
		}
	}
}